import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient, } from '@angular/common/http';
import { Router } from '@angular/router'
import Swal from 'sweetalert2';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css'],
  providers:[HttpClient]
})
export class SignInComponent implements OnInit { 

  email;
  privateKey;
  
  constructor(private http: HttpClient,
              private router : Router) { }

  ngOnInit() {
  }

  showModal(){
    Swal.fire({
      icon:'error',
      title:'Login Failed',
      text:'Please Check your Email or Private Key'      
    }).then(()=>{ })
  }

  Login(){
    let obj = {    
      email: this.email,
      privateKey: this.privateKey
    }        
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    this.http.post<any>('http://localhost:3000/signin',obj,{headers:headers}).subscribe(
      (res)=>{        
        if(res.msg == 'Login Success'){
          console.log(res);
          localStorage.setItem('privateKey',res.privateKey)
          localStorage.setItem('Eth_Account',res.Eth_Account)
          this.router.navigate(['/dashboard'])
        }
        else if(res.msg == 'Login Failed'){
          this.showModal();
        }
        else{
          alert(res.msg);
        }
      },
      (err) => console.log(err)
    );
  }
}
