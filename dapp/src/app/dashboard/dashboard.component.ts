import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  toggle = true;
  togglePatientDetails = false;
  record;

  constructor() { }

  ngOnInit() {
  }
  
  toggleEventForRegister(){
    this.toggle = false;
  }

  togglePatient(event){
    console.log(event)
    this.record = event
    this.togglePatientDetails = event.togglePatientDetails;    
  }
}
