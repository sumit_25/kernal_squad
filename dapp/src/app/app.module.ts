import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule} from './material/material.module';
import { SignInComponent } from './sign-in/sign-in.component';
import { HomePageComponent } from './home-page/home-page.component';
import { MedicalReportComponent } from './medical-report/medical-report.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RegisterPatientComponent } from './register-patient/register-patient.component';
import { GetPatientComponent } from './get-patient/get-patient.component';
import { PatientDetailsComponent } from './patient-details/patient-details.component'


@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    SignInComponent,
    HomePageComponent,
    MedicalReportComponent,
    DashboardComponent,
    RegisterPatientComponent,
    GetPatientComponent,
    PatientDetailsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    FormsModule,
    SweetAlert2Module.forRoot()  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
