import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './signup/signup.component'
import { SignInComponent } from './sign-in/sign-in.component'
import { HomePageComponent } from './home-page/home-page.component'
import { MedicalReportComponent } from './medical-report/medical-report.component'
import { DashboardComponent } from './dashboard/dashboard.component'

const routes: Routes = [
  {path:'',component:HomePageComponent},
  {path:'signin', component:SignInComponent},
  {path:'signup',component:SignupComponent},
  {path:'home',component:HomePageComponent},
  {path:'report',component:MedicalReportComponent},
  {path:'dashboard',component:DashboardComponent},  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
