import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpHeaders, HttpClient, } from '@angular/common/http';
import { Router } from '@angular/router'

@Component({
  selector: 'app-get-patient',
  templateUrl: './get-patient.component.html',
  styleUrls: ['./get-patient.component.css']
})
export class GetPatientComponent implements OnInit {

  uid;
  togglePatientDetails=false;
  @Output() msgEvent = new EventEmitter<boolean>();

  constructor(private http: HttpClient,
              private router : Router) { }

  ngOnInit() {
  }

  getPatientRecord(){
    let obj = {    
      uid: this.uid,
      privateKey:localStorage.getItem('privateKey'),
      Eth_Account:localStorage.getItem('Eth_Account')
    }        
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    this.http.post<any>('http://localhost:3000/getRecords',obj,{headers:headers}).subscribe(
      (res)=>{        
        if(res.msg == 'success'){
          console.log(res);
          res.togglePatientDetails = true;          
          this.msgEvent.emit(res)
        }        
        else{
          alert(res.msg);
          this.togglePatientDetails = false;
        }
      },
      (err) => console.log(err)
    );
  }

}
