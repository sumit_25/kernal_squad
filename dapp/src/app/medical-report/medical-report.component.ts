import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-medical-report',
  templateUrl: './medical-report.component.html',
  styleUrls: ['./medical-report.component.css'],
  providers: [HttpClient,FormBuilder]
})
export class MedicalReportComponent implements OnInit {

  images;
  title = 'fileUpload';

  constructor(private http: HttpClient, private form: FormBuilder) { }

  ngOnInit() {    
  }

  selectImage(event){
    if(event.target.files.length > 0){
      const file = event.target.files[0];
      this.images = file;
    }
  }

  onSubmit(){
    const formData = new FormData();
    formData.append('file',this.images);
    this.http.post<any>('http://localhost:3000/report',formData).subscribe(
      (res)=>console.log(res),
      (err) => console.log(err)            
    );
  }
}
