import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpHeaders, HttpClient, } from '@angular/common/http';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register-patient',
  templateUrl: './register-patient.component.html',
  styleUrls: ['./register-patient.component.css'],
  providers:[HttpClient]
})
export class RegisterPatientComponent implements OnInit {

  name;
  aadhar;
  contact_no;
  age;
  gender;

  constructor(private http : HttpClient) { }

  ngOnInit() {

  }

  showModal(res: any){
    if(res.msg == 'success'){
      Swal.fire({
        icon: 'success',
        title:'Patient Registered', 
        html:"Patient's <strong>Unique ID</strong> is : <strong>" + res.uid + "</strong><br>(Please remeber this)",                       
        //text:'Your private key is : ' + res.privatekey + '<br>Your Eth_key is : ' + res.Eth_Account, 
      })
    }
    else{
      Swal.fire({
        icon: 'error',
        title:'Error Occurred',        
        position : 'top-right',
        timer : 2000
        //text:'Your private key is : ' + res.privatekey + '<br>Your Eth_key is : ' + res.Eth_Account, 
      })
    }        
  }

  Register(){
    let patient = {
      name : this.name,
      aadhar : this.aadhar,
      contact_no : this.contact_no,
      age : this.age,
      gender : this.gender,
      privateKey: localStorage.getItem('privateKey'),
      Eth_Account: localStorage.getItem('Eth_Account')
    }
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    this.http.post<any>('http://localhost:3000/register_patient',patient,{headers:headers}).subscribe(
      (res)=>{
        console.log(res);
        this.showModal(res);
        this.toggleEventForRegister.emit(false);
      },
      (err) => console.log(err)
    );
  }

}
