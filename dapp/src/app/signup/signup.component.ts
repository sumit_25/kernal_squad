import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient, } from '@angular/common/http';
import { Router } from '@angular/router'
import Swal from 'sweetalert2';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers:[HttpClient]
})
export class SignupComponent implements OnInit {

 
  dep = [
    "Cardiology",
    "ENT",
    "Neurology",
    "Opthalmology",
    "Orthopedics",
    "Physiotherapy",
  ]

  departments = [];
  name;
  address;
  email;
  contact_no;  

  constructor(private http: HttpClient,
              private router : Router) { }

  ngOnInit() {
  }


  showModal(res: any){
    Swal.fire({
      icon:'success',
      title:'Congratulations, Your account has been created!',
      html : 'Your private key is : <strong>' + res.privatekey + '</strong><br><br>Your Eth_key is : <strong>' + res.Eth_Account + '</strong>',
      //text:'Your private key is : ' + res.privatekey + '<br>Your Eth_key is : ' + res.Eth_Account, 
    }).then(()=>{
      this.router.navigate(['/signin']);
    })
  }

  Submit(){    
    let obj = {
      name: this.name,
      address: this.address,
      email: this.email,
      contact_no: this.contact_no,
      departments: this.departments,
    }        
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    this.http.post<any>('http://localhost:3000/signup',obj,{headers:headers}).subscribe(
      (res)=>{
        console.log(res);
        this.showModal(res);
      },
      (err) => console.log(err)
    );
    console.log(obj);
  }
}
