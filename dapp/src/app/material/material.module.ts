import { NgModule } from '@angular/core';
import {
  MatToolbarModule,MatFormFieldModule,
   MatInputModule, MatSelectModule,
    MatCheckboxModule, MatDatepickerModule, MatNativeDateModule, MatButtonModule
} from '@angular/material';

const material = [
  MatToolbarModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatButtonModule
]

@NgModule({
  declarations: [],
  imports: [material],
  exports: [material]
})
export class MaterialModule { }
